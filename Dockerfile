FROM openjdk:7-jre-alpine
COPY /target .
COPY run.sh /run.sh
RUN chmod +x /run.sh
EXPOSE 8080
RUN ./run.sh
